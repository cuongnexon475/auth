import { Alert, Box, Typography } from "@mui/material";
import { ChangeEvent, FormEvent, useMemo, useState } from "react";
import { toast } from "react-hot-toast";

import authApi from "@/api/modules/auth.modules";
import { useModalContext } from "@/providers/modalProvider";
import { TAuth } from "@/types/auth";
import { storage } from "@/utils/storage";
import CustomButton from "@/components/button";
import InputField from "@/components/inputField";
import { LOGIN_SUCCESS, USER_NOT_FOUND } from "@/configs/notification";
import { TResponse } from "@/types/response";
import { ERROR_PORT_USER_NOT_FOUND } from "@/configs/appConfigs";
import { comparison } from "@/utils/comparison";

const SignInModal = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const [error, setErorr] = useState("");

  const { onClose } = useModalContext();

  const handleChangeUsername = (event: ChangeEvent<HTMLInputElement>) => {
    setUsername(event.target.value);
  };

  const handleChangePassword = (event: ChangeEvent<HTMLInputElement>) => {
    setPassword(event.target.value);
  };

  const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setErorr("");

    const userData: TAuth = { username, password };
    const response: TResponse = await authApi.signin(userData);

    if (response?.data) {
      toast.success(LOGIN_SUCCESS);

      storage.setToken(response?.data.token as string);
      storage.setUsername(response?.data.userSignInfo?.username as string);

      onClose();
    }

    if (
      comparison(
        response.error?.response?.status as number,
        ERROR_PORT_USER_NOT_FOUND
      )
    ) {
      setErorr(USER_NOT_FOUND);
    }
  };

  const sxButton = useMemo(() => {
    return { paddingX: 8, marginTop: 2 };
  }, []);

  return (
    <Box component={"form"} mt={2} onSubmit={handleSubmit}>
      <Box display="flex" alignItems="center" gap={2}>
        <Typography variant="body1" width="100px">
          Username
        </Typography>
        <InputField
          label="Username"
          value={username}
          setValue={handleChangeUsername}
        />
      </Box>

      <Box mt={2} display="flex" alignItems="center" gap={2}>
        <Typography variant="body1" width="100px">
          Password
        </Typography>
        <InputField
          label="Password"
          value={password}
          setValue={handleChangePassword}
          type="password"
        />
      </Box>

      {error && (
        <Alert sx={{ marginTop: 2 }} severity="error">
          {error}
        </Alert>
      )}

      <Box display="flex" justifyContent="center">
        <CustomButton label="Sign in" sx={sxButton} isSubmit={true} />
      </Box>
    </Box>
  );
};

export default SignInModal;
