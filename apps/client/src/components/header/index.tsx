import SlideBottomBox from "@/animations/SlideBottomBox";
import { useModalContext } from "@/providers/modalProvider";
import { storage } from "@/utils/storage";
import { Box, Typography } from "@mui/material";
import React from "react";
import CustomButton from "../button";
import UserList from "../userList";

const Header = () => {
  const { onOpen, setTypeModal } = useModalContext();

  const token = storage.getToken();
  const username = storage.getUsername();

  const handleSignIn = () => {
    onOpen();
    setTypeModal("signin");
  };

  const handleSignUp = () => {
    onOpen();
    setTypeModal("signup");
  };

  const handleLogout = () => {
    storage.clearToken();
    storage.clearUsername();

    location.reload();
  };

  return (
    <Box
      display="flex"
      alignItems="center"
      justifyContent="flex-start"
      paddingX="8px"
      gap="8px"
    >
      {!token ? (
        <>
          <CustomButton
            sx={{ paddingBottom: "4px", paddingX: "24px" }}
            label="Sign in"
            onClick={handleSignIn}
          />

          <CustomButton
            sx={{ paddingBottom: "4px", paddingX: "24px" }}
            label="Sign up"
            onClick={handleSignUp}
          />
        </>
      ) : (
        <Box>
          <Box
            display="flex"
            alignItems="center"
            justifyContent="center"
            gap={4}
          >
            <CustomButton
              sx={{ paddingBottom: "4px", paddingX: "24px" }}
              label="Logout"
              onClick={handleLogout}
            />

            <Typography
              variant="body2"
              fontSize={"20px"}
            >{`Welcome ${username}`}</Typography>
          </Box>

          <SlideBottomBox>
            <UserList />
          </SlideBottomBox>
        </Box>
      )}
    </Box>
  );
};
export default React.memo(Header);
