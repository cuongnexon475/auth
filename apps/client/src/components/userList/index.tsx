import { useEffect, useState } from "react";

import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import Avatar from "@mui/material/Avatar";
import { BsPersonFill } from "react-icons/bs";
import { TUser } from "@/types/user";

import authApi from "@/api/modules/auth.modules";
import { TReponseUsers } from "@/types/response";

function UserList() {
  const [users, setUsers] = useState<TUser[]>([]);

  useEffect(() => {
    const getUsers = async () => {
      const response: TReponseUsers = await authApi.getListUsers();
      setUsers(response.data?.userList as TUser[]);
    };

    getUsers();
  }, []);

  return (
    <List sx={{ width: "100%", maxWidth: 360, bgcolor: "grey.300" }}>
      {users?.map((user: TUser) => (
        <ListItem key={user.id}>
          <ListItemAvatar>
            <Avatar>
              <BsPersonFill />
            </Avatar>
          </ListItemAvatar>
          <ListItemText primary={user.username} />
        </ListItem>
      ))}
    </List>
  );
}

export default UserList;
