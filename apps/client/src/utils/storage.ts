const storagePrefix = "auth_";

export const storage = {
  getToken: () => {
    const token = JSON.parse(
      window.localStorage.getItem(`${storagePrefix}token`) as string
    ) as string;
    return token;
  },
  setToken: (token: string) => {
    window.localStorage.setItem(`${storagePrefix}token`, JSON.stringify(token));
  },
  clearToken: () => {
    window.localStorage.removeItem(`${storagePrefix}token`);
  },

  setUsername: (username: string) => {
    window.localStorage.setItem(
      `${storagePrefix}user`,
      JSON.stringify(username)
    );
  },
  getUsername: () => {
    const username = window.localStorage.getItem(`${storagePrefix}user`);
    return JSON.parse(username as string);
  },
  clearUsername: () => {
    window.localStorage.removeItem(`${storagePrefix}user`);
  },
};
