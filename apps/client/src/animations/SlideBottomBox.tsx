import { Box, keyframes } from "@mui/material";
import styled from "@mui/material/styles/styled";
import { ReactNode } from "react";

const boxSlideBottom = keyframes`
0% {
    -webkit-transform: translateY(-20px);
            transform: translateY(-20px);
  }
  100% {
    -webkit-transform: translateY(20px);
            transform: translateY(20px);
  }
`;

const Holder = styled(Box)(() => ({
  animation: `${boxSlideBottom} 1s ease-out both`,
}));

interface ISildeBottomBox {
  children: ReactNode;
}

const SlideBottomBox = ({ children }: ISildeBottomBox) => {
  return <Holder mt={2}>{children}</Holder>;
};

export default SlideBottomBox;
