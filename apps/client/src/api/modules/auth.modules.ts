import { TAuth } from "@/types/auth";
import { axiosInstance } from "@/utils/axios";

const authEndpoints = {
  signin: "/user/login",
  signup: "/user/generateAccount",
  listUsers: "/user/user-list",
};

const authApi = {
  signin: async (body: TAuth) => {
    try {
      const data = await axiosInstance.post(authEndpoints.signin, body);
      return data;
    } catch (error: any) {
      return { error };
    }
  },

  signup: async (body: TAuth) => {
    try {
      const data = await axiosInstance.post(authEndpoints.signup, body);
      return data;
    } catch (error: any) {
      return { error };
    }
  },

  getListUsers: async () => {
    try {
      const data = await axiosInstance.get(authEndpoints.listUsers);
      return data;
    } catch (error: any) {
      return { error };
    }
  },
};

export default authApi;
