export type TUser = {
  username?: string;
  id?: string;
  email?: string | null;
};
